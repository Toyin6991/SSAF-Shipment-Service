import re

from email_client.exceptions.ssaf_email_exceptions import SMTPInvalidEmailAddressException


def validate_email(emails: list = None):
    """this function validates that each email address has been correctly parsed from the list of emails provided"""
    EMAIL_REGEX = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")

    if len(emails) == 1:
        if not EMAIL_REGEX.search(emails[0]):
            raise SMTPInvalidEmailAddressException(emails[0])

    for email in emails:
        if not EMAIL_REGEX.search(email):
            raise SMTPInvalidEmailAddressException(email)


def email_parser(email: str) -> list:
    """takes an email string and returns a list containing the email"""
    parsed_email = [email]
    return parsed_email
