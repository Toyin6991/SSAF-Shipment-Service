from typing import List, Optional
from pydantic import BaseModel, EmailStr, Extra


class Mail(BaseModel):
    subject: str
    sender: str  # EmailStr was throwing some unexpected error
    clients: List[str]
    message: str
    attachment: Optional[List[str]] = None

    class Config:
        extra = Extra.forbid


class SSAFMail(BaseModel):
    mails: List[Mail]
