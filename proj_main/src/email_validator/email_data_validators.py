"""This module validates and checks for the correctness of data passed into the ssaf_email_client"""

from email_client.exceptions.ssaf_email_exceptions import SMTPLoginException, \
    SMTPDataConstraintException


def validate_data(data: dict = None):
    if not data.get('subject', ""):
        raise SMTPLoginException('subject')
    if not isinstance(data.get('subject', None), str):
        raise SMTPDataConstraintException('Subject', 'str')
    if not data.get('sender', ""):
        raise SMTPLoginException('sender')
    if not isinstance(data.get('sender', None), str):
        raise SMTPDataConstraintException('From', 'str')
    if not data['clients']:
        raise SMTPLoginException('clients')
    if not isinstance(data.get('clients', ""), list):
        raise SMTPDataConstraintException('To', 'list')
