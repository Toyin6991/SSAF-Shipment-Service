class SMTPInvalidEmailAddressException(Exception):
    def __init__(self, email_value: str):
        message = f"Got an invalid email address: {email_value}"
        super().__init__(message)


class SMTPConnectionException(Exception):
    def __init__(self, value: str = None):
        message = f"Couldn't initiate a SMTP server: {value}"
        super().__init__(message)


class SMTPConfigException(Exception):
    def __init__(self, value: str = None):
        message = f"Couldn't load the following environment variable: {value}"
        super().__init__(message)


class SMTPLoginException(Exception):
    def __init__(self, value: str = None):
        message = f"The data is lacking the '{value}' field which is required to send email"
        super().__init__(message)


class SMTPDataConstraintException(Exception):
    def __init__(self, value: str, data_type: str):
        message = f"The '{value}' field must be a {data_type}"
        super().__init__(message)


class SMTPFileNotFoundException(Exception):
    def __init__(self, value: str = None):
        message = f"{value}"
        super().__init__(message)


class SMTPInstanceError(Exception):
    def __init__(self, *args):
        if args:
            super().__init__(args)
        else:
            super().__init__("You did not provide a valid data")
