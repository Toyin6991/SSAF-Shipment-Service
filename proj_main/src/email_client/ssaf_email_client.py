"""
This module sends email to given client()s using the following steps:

1. Set up the SMTP server and log into your account.
2. Create the MIMEMultipart message object and load it with appropriate headers for From, To,
and Subject fields.
3. Add your message body.
4. Send the message using the SMTP server object.
"""
import smtplib
import ssl
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from os.path import basename

from decouple import config

from data_pydantic.email_data_pydantic import SSAFMail
from email_client.exceptions.ssaf_email_exceptions import SMTPConnectionException, \
    SMTPInvalidEmailAddressException, SMTPLoginException, SMTPDataConstraintException, SMTPConfigException, \
    SMTPFileNotFoundException, SMTPInstanceError
from email_validator.email_regex import email_parser


class SsafSmtpClient:
    """
    :desc: A simple email client that processes a list of list of SSAFMail
    :params: It expects a list of SSAFMail
    :return: email receipt or exception if wrong data were passed
    """
    _values = None
    _smtp = None

    def __init__(self, data):
        self.data = SSAFMail(mails=data)

        try:
            if not SsafSmtpClient._values:
                SsafSmtpClient._values = SsafSmtpClient._smtp_config_loader()

            if not SsafSmtpClient._smtp or not SsafSmtpClient._test_conn_open(SsafSmtpClient._smtp):
                SsafSmtpClient._smtp = SsafSmtpClient._connect()
                SsafSmtpClient._login()
        except SMTPConfigException as e:
            print(e.__str__())
        except SMTPConnectionException as e:
            print(e.__str__())

    @staticmethod
    def _smtp_config_loader():
        try:
            _email = email_parser(config('EMAIL'))
            env_data = {
                'email': _email[0],
                'password': config('PASSWORD')
            }

            for key in env_data:
                if not env_data[key]:
                    raise SMTPConfigException(key)
            return env_data

        except SMTPInvalidEmailAddressException as e:
            print(e.__str__())

    @classmethod
    def _connect(cls):
        # Create a secure SSL context
        context = ssl.create_default_context()

        try:
            conn = smtplib.SMTP(host=config('HOST'), port=config('PORT'), timeout=30)
        except smtplib.SMTPConnectError as e:
            raise SMTPConnectionException(e.__str__())
        except TimeoutError as e:
            print(e.__str__())
        except Exception as e:
            print(e.__str__())
        else:
            conn.starttls(context=context)  # Secure the connection
            return conn

    @classmethod
    def _test_conn_open(cls, conn):
        status = False
        try:
            status = conn.noop()[0]
        except smtplib.SMTPServerDisconnected as e:
            print(e.__str__())
        except Exception as e:
            print(e.__str__())
        finally:  # i believe this should be else
            return status == 250

    @classmethod
    def _login(cls):
        try:
            cls._smtp.ehlo()
        except smtplib.SMTPServerDisconnected as e:
            print(e.__str__())
        else:
            SsafSmtpClient._smtp.login(user=SsafSmtpClient._values.get("email", ""),
                                       password=SsafSmtpClient._values.get("password", ""))  # logging in to server

    def _add_attachment(self, _msg, data: dict):
        try:
            for f in data['attachment']:
                with open(f, "rb") as file_obj:
                    part = MIMEApplication(
                        file_obj.read(), Name=basename(f)
                    )
                # After the file is closed
                part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
                _msg.attach(part)
        except FileNotFoundError as ex:
            raise SMTPFileNotFoundException(ex.__str__())
        except FileExistsError as ex:
            raise SMTPFileNotFoundException(ex.__str__())
        except Exception as ex:
            print(ex.__str__())

    def _ssaf_emailer(self, data: dict = None):
        try:
            # still testing this part to not reveal the other mail recipients
            _msg = MIMEMultipart()
            _msg['Subject'] = data['subject']
            _msg['From'] = data['sender']  # or self.email
            _msg['To'] = ",".join(data['clients'])  # it still goes but the 'To' field in Gmail is left blank
            _msg['Date'] = formatdate(localtime=True)

            # attaching the text
            _msg.attach(MIMEText(data['message']))

            if data['attachment']:
                self._add_attachment(_msg, data=data)

            SsafSmtpClient._smtp.sendmail(data['sender'], data['clients'], _msg.as_string())  # sending email
            # self.smtp.send_message(msg=self.msg, from_addr=data['sender'], to_addrs=data['clients'])

        except SMTPInvalidEmailAddressException as ex:
            print(ex.__str__())  # logger logs the exception
        except SMTPDataConstraintException as ex:
            print(ex.__str__())  # logger logs the exception
        except SMTPLoginException as ex:
            print(ex.__str__())  # logger logs the exception
        except SMTPFileNotFoundException as ex:
            print(ex.__str__())
        except smtplib.SMTPHeloError as ex:
            print(ex.__str__())
        except smtplib.SMTPRecipientsRefused as ex:
            print(ex.__str__())
        except smtplib.SMTPSenderRefused as ex:
            print(ex.__str__())
        except smtplib.SMTPDataError as ex:
            print(ex.__str__())
        except smtplib.SMTPNotSupportedError as ex:
            print(ex.__str__())
        except Exception as ex:
            print(ex.__str__())

    def send_mail(self):
        try:
            for data in self.data.mails:
                self._ssaf_emailer(data=data.dict())
        except SMTPInstanceError as ex:
            print(ex.__str__())
        except smtplib.SMTPConnectError as e:
            SsafSmtpClient._connect()
            SsafSmtpClient._login()
            for data in self.data.mails:
                self._ssaf_emailer(data=data.dict())
        except smtplib.SMTPServerDisconnected as e:
            SsafSmtpClient._connect()
            SsafSmtpClient._login()
            for data in self.data.mails:
                self._ssaf_emailer(data=data.dict())
        except Exception as e:
            print(e.__str__())
        else:
            SsafSmtpClient._smtp.quit()  # close server connection
            return "success"
