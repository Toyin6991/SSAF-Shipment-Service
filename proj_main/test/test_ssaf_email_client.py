from unittest import TestCase

from email_client.ssaf_email_client import SsafSmtpClient


class SSAFEmailClientTest(TestCase):

    def setUp(self) -> None:
        self.data = [
            {
                "subject": "TEST MAIL",
                "sender": 'hello@ssafllc.com',
                "clients": ["thehealthstuff96@gmail.com", "ONAGORUWAM@YAHOO.COM"],
                "message": "A DIFFERENT MAIL",
                "attachment": [
                    "C:/Users/ADMIN/OneDrive - University of Lagos/Documents/SSAF_EMAIL_CLIENT/proj_main/src/example.txt"]
            }
        ]

        self.email_client_test = SsafSmtpClient(self.data)

    def test_smtp_client_can_send_email_successfully_upon_creation(self):
        status = self.email_client_test.send_mail()
        self.assertEqual(status, "success")

    def test_smtp_email_client_can_send_mail_without_attachment(self):
        status = self.email_client_test.send_mail()
        self.assertEqual(status, "success")
