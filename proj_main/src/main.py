from email_client.ssaf_email_client import SsafSmtpClient

my_data = [
    {
        "subject": "TESTING BCC",
        "sender": 'hello@ssafllc.com',
        "clients": ["thehealthstuff96@gmail.com", "onagoruwam@yahoo.com"],
        "message": "THIS IS A DEMO",
        "attachment": [
            "C:/Users/ADMIN/OneDrive - University of Lagos/Documents/SSAF_EMAIL_CLIENT/proj_main/src/example.txt"]
    },
    {
        "subject": "TESTING BCC",
        "sender": 'hello@ssafllc.com',
        "clients": ["toyin.onagoruwa@ssafllc.com"],
        "message": "SECOND DEMO MAIL",
        "attachment": [
            "C:/Users/ADMIN/OneDrive - University of Lagos/Documents/SSAF_EMAIL_CLIENT/proj_main/src/example.txt"]
    }
]

if __name__ == "__main__":
    my_client = SsafSmtpClient(my_data)
    result = my_client.send_mail()
    print(result)
